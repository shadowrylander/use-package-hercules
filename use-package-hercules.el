;;; use-package-hercules.el --- Adds :hercules keyword to use-package macro

;; Copyright (C) 2018 Toon Claes

;; Author: Toon Claes <toon@iotcl.com>
;; Maintainer: Toon Claes <toon@iotcl.com>
;; Created: 6 Jan 2018
;; Modified: 18 Aug 2018
;; Version: 0.1
;; Package-Requires: ((emacs "24.3") (use-package "2.4"))
;; Keywords: convenience extensions tools
;; URL: https://gitlab.com/to1ne/use-package-hydra

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; Provides support for the :hercules keyword, which is made available by
;; default by requiring `use-package'.

;;; Code:

(require 'use-package-core)

(defun use-package-hercules--name (name)
  "Build hercules name for the package NAME."
  (cl-gentemp (concat "hercules-" (symbol-name name))))

(defun use-package-hercules--normalize (name _keyword args)
  "Normalize the ARGS to be a list hydras.
It accepts a single hercules, or a list of hydras.  It is optional
provide a name for the hercules, if so there is a name generated
from NAME."
  (let (name result*)
    (while args
      (cond
       ;; single named hercules
       ((symbolp (car args))
        (setq result* (nconc result* (list args))
              args nil))
       ;; single unnamed hercules with docstring
       ((stringp (nth 2 args))
        (setq name (use-package-hercules--name name)
              result* (nconc result* (list (cons name args)))
              args nil))
       ;; TODO single unnamed hercules without docstring

       ;; list of hydras
       ((listp (car args))
        (setq result*
              (nconc result* (use-package-hercules--normalize name _keyword (car args)))
              args (cdr args)))
       ;; Error!
       (t
        (use-package-error
         (concat (symbol-name name)
                 " wants arguments acceptable to the `hercules-def' macro,"
                 " or a list of such values")))))
    result*))

;;;###autoload
(defalias 'use-package-normalize/:hercules 'use-package-hercules--normalize
  "Normalize for the definition of one or more hydras.")

;;;###autoload
(defun use-package-handler/:hercules (name keyword args rest state)
  "Generate hercules-def with NAME for `:hercules' KEYWORD.
ARGS, REST, and STATE are prepared by `use-package-normalize/:hercules'."
  (use-package-concat
   (mapcar #'(lambda (def) `(hercules-def ,@def))
           args)
   (use-package-process-keywords name rest state)))

(add-to-list 'use-package-keywords :hercules t)

(when (boundp 'use-package-deferring-keywords)
    (add-to-list 'use-package-deferring-keywords :hercules t))

(provide 'use-package-hercules)
;;; use-package-hercules.el ends here
